>> General Info
	- Language: Python (3.6.5)
	- Dependency: Pillow (5.2.0)

>> Major Design Choices
	# Approach to get a list of available filters taking into account the image type
		- In order to accomplish that, I've chosen to add a static variable to each filter, called image_types, that will hold a list of image types that can be manipulated by that filter. The choice for a list instead of a string is based on the possibility that maybe some filter can be applied to multiple image types. In our case, both the RGB filter and the blur filter only have a single image type, 'RGB', but to prepare for other potential developments, we defined the variable as a list.

	# Approach to make the process of filter addition to be as easy as possible for other developers.
		- Basically, the whole process of creating a new filter is based on creating a new class that extends the base class Filter, filling the necessary data (name, image type, extra parameters required) and then implementing the 'apply' method. That's pretty much it. I wanted to make as easy as possible for other developers to create new filters and to accomplish that I, I made sure the developer didn't have to worry about the the input of the new filter parameters through the command line, in case they are needed, and also the interactive input of those parameters. So I tried to abstract these points the best I could.

>> Steps to create a new filter

1. Create a class that extends from the abstract class Filter. 
	- You can place the class anywhere you want, but I recommend to create a new file in the folder filters, that will be the place for your new class.

2. Set the static attributes bellow
	- name: The name of your filter

	- command_line_flag: to be used as a requirement for the command line, as in '-blur' or '-flag'. Make sure to set without the dash. So, for example, if we're creating a grayscale filter, it should be 'grayscale', not '-grayscale'.

	- image_types: list of image types that can be manipulated by that filter. See https://pillow.readthedocs.io/en/5.1.x/handbook/concepts.html#concept-modes for a full list of possible image types.

	- extra_data_required: In case the filter needs extra parameters, we will set the data in this variable. For example, the blur filter needs radius and weight. The extra_data_required is expected to be a list of dictionaries, where each dictionary has 2 keys:
			- 'attribute_name': The name of the attribute. Ex: 'radius', 'weight'
			
			- 'callback_function': The function that will be called to validate the parameter and return a valid value. See example in filters/blur_filter.py. The application supports static methods as well. This function should receive a raw string as input, coming either from the command line or an interactive input, perform the necessary validations and convertions and return a valid value for that parameter.

3. Implement the abstract method 'apply'
	- Don't forget to call super().apply() as the the first line on your method.

4. In main.py, import the class you just created.

And that's it.

Now, you're able to call:

python main.py -grayscale image.png

And the file will be generated.

The grayscale option has also been added to the list of available filters to choose on interactive input.
