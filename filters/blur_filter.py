from filters.filter import Filter
from PIL import Image


class BlurFilter(Filter):
    name = "Blur"
    command_line_flag = 'blur'
    image_types = ['RGB']

    def apply(self):
        super().apply()

        self.create_image_with_blur_filter(self.filter_parameters['radius'], self.filter_parameters['weight'])

        print('>> The new image with blur filter was saved in the directory of the original image, at {} <<'.format(
            self.image.filename))

    def create_image_with_blur_filter(self, radius: int, weight: int) -> None:
        """This method creates and saves a new image file, after applying a blur filter on the original image

        :param image: The original image
        :param radius: An integer value that determines the surrounding pixels from where color average is calculated
        :param weight: ​A weight to be applied to the central pixel only (all other pixels take the weight 1),
        """
        width, height = self.image.size

        new_img = Image.new(self.image.mode, (width, height))
        pixels_new_img = new_img.load()  # create the pixel map

        # As the first step, we create a matrix containg the RGB values for the original image
        rgb_matrix = [[self.image.getpixel((i, j)) for j in range(height)] for i in range(width)]

        for row_number in range(width):
            for column_number in range(height):
                # We get a list of the neighbours of the cell, excluding the cell itself, which we'll add later to calculate
                # the weighted average
                neighbours_values = BlurFilter.get_neighbours_value_of_cell(rgb_matrix, row_number, column_number,
                                                                            radius, include_cell=False)

                # The order of this list is important because of the indexes
                channels = ['red', 'green', 'blue']
                weighted_neighbours_values_by_channel = {}

                for i, channel in enumerate(channels):
                    channel_values = [x[i] for x in neighbours_values]

                    # Every neighbourd receives a weight of 1
                    weighted_channel_values = [{'weight': 1, 'value': value} for value in channel_values]

                    # The cell itself receives the weight specified as the input argument
                    weighted_channel_values.append(
                        {'weight': weight, 'value': rgb_matrix[row_number][column_number][i]})

                    weighted_neighbours_values_by_channel[channel] = weighted_channel_values

                # The new pixel is a rounded weighted average calculated separately for each channel
                pixels_new_img[row_number, column_number] = (
                    BlurFilter.get_rounded_weighted_average_from_list(weighted_neighbours_values_by_channel['red']),
                    BlurFilter.get_rounded_weighted_average_from_list(weighted_neighbours_values_by_channel['green']),
                    BlurFilter.get_rounded_weighted_average_from_list(weighted_neighbours_values_by_channel['blue']))

        self.save_image(new_img, "blur_radius_{}_weight_{}".format(radius, weight))

    @staticmethod
    def get_neighbours_value_of_cell(matrix: list, row_number: int, column_number: int, radius: int = 1,
                                     include_cell: bool = True):
        """This method will get all of the neighbours values from a single cell

        :param matrix: A matrix to extract the neighbours from
        :param row_number: The row number of the cell that is the reference point
        :param column_number: The column number of the cell that is the reference point
        :param radius: An integer positive value to be used as the radius
        :param include_cell: A boolean to represent if the reference cell should be included in the list of neighbours
        """
        neighbours_list = []

        num_rows, num_columns = len(matrix), len(matrix[0])

        # The loops goes from N rows above the reference points, where N is the radius and, to N rows below the cell
        for i in range(row_number - radius, row_number + 1 + radius):
            # The loops goes from N columns to the left of the cell to N column to the right of the cell, where N is the
            # radius
            for j in range(column_number - radius, column_number + 1 + radius):
                if 0 <= i < num_rows and 0 <= j < num_columns:
                    if i == row_number and j == column_number:
                        if include_cell:
                            neighbours_list.append(matrix[i][j])
                    else:
                        neighbours_list.append(matrix[i][j])

        return neighbours_list

    @staticmethod
    def get_rounded_weighted_average_from_list(list_nums: list) -> int:
        sum_of_weights = sum(item['weight'] for item in list_nums)

        return round(sum(item['weight'] * item['value'] for item in list_nums) / sum_of_weights)

    @staticmethod
    def get_radius_value(raw_radius_value: str) -> int:
        try:
            radius = int(raw_radius_value)
        except (ValueError, IndexError) as e:
            raise ValueError('Radius must be an integer')

        if radius < 1:
            raise ValueError('Radius must be at least 1')

        return radius

    @staticmethod
    def get_weight_value(raw_weight_value: str) -> float:
        try:
            weight = float(raw_weight_value)
        except (ValueError, IndexError):
            raise ValueError('Weight must be a float')

        if weight < 1:
            raise ValueError('weight must be an float number equal or greater than 1.')

        return weight

    # We put the extra data required down here, so we can refer to both methods (get_radius_value, get_weight_value) and
    # this reference can only happen after the function definition.
    extra_data_required = [
        {'attribute_name': 'radius', 'callback_function': get_radius_value},
        {'attribute_name': 'weight', 'callback_function': get_weight_value},
    ]
