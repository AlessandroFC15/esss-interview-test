import os
from filters.filter import Filter


class GrayscaleFilter(Filter):
    name = "Grayscale"
    command_line_flag = "grayscale"
    image_types = []
    extra_data_required = []

    def apply(self):
        grayscale_img = self.image.convert('LA')

        grayscale_img.save('{}_{}.png'.format(os.path.splitext(self.image.filename)[0], 'grayscale'))