from filters.filter import Filter
from PIL import Image


class RGBSplitFilter(Filter):
    name = "RGB Split"
    command_line_flag = 'rgb'
    image_types = ['RGB']

    def apply(self):
        super().apply()

        color_channels = ['red', 'green', 'blue']

        for color_channel in color_channels:
            self.create_image_with_single_RGB_color_channel(color_channel)

        print('>> All 3 images were saved in the directory of the original image, at {} <<'.format(self.image.filename))

    def create_image_with_single_RGB_color_channel(self, color_channel: str) -> None:
        """ This method creates and saves a new image file, having a single RGB color channel from the original image.

        :param original_img: The original image
        :param color_channel: A lowercase string representing one of 'red', 'green' or 'blue'

        Raises ValueError if the color channel is not among the predefined values.
        """
        if color_channel not in ['red', 'green', 'blue']:
            raise ValueError("Color channel must be one of: 'red', 'green' or 'blue'")

        width, height = self.image.size

        new_img = Image.new(self.image.mode, (width, height))
        pixels_new_img = new_img.load()  # create the pixel map

        for i in range(width):
            for j in range(height):
                pixel_data = self.image.getpixel((i, j))

                if len(pixel_data) == 3:
                    r, g, b = self.image.getpixel((i, j))
                elif len(pixel_data) == 4:
                    r, g, b, a = self.image.getpixel((i, j))

                if color_channel == "red":
                    pixels_new_img[i, j] = (r, 0, 0)
                elif color_channel == "green":
                    pixels_new_img[i, j] = (0, g, 0)
                else:
                    pixels_new_img[i, j] = (0, 0, b)

        self.save_image(new_img, color_channel)