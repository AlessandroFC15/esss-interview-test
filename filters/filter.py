import os
from abc import ABC, abstractmethod
from PIL import Image


class Filter(ABC):
    name = NotImplemented

    # This will be be used as a requirement for the command line app, as in '-blur' or '-flag'.
    # Make sure to set without the dash. So, for example, if we're creating a grayscale filter, it should be 'grayscale'
    # , not '-grayscale'.
    command_line_flag = NotImplemented

    # List of image types that can be manipulated by that filter. Check
    # https://pillow.readthedocs.io/en/5.1.x/handbook/concepts.html#concept-modes for a full list of possible image
    # types.
    image_types = NotImplemented

    # In case the filter needs extra parameters, the data should be declared in this variable. For example, the blur
    # filter needs radius and weight. The extra_data_required is expected to be a list of dictionaries, where each
    # dictionary has 2 keys:
    #   'attribute_name': The name of the attribute. Ex: 'radius', 'weight'
    #   'callback_function': The function that will be called to validate the parameter and return a valid value. 7
    #       See example in filters/blur_filter.py. The application supports static methods as well. This function should
    #       receive a raw string as input, coming either from the command line or an interactive input, perform the
    #       necessary validations and conversions and return a valid value for that parameter.
    extra_data_required = []

    def __init__(self, image: Image):
        if image.mode not in self.image_types:
            raise ValueError(">> {} filter can not be applied to image with type {}".format(self.name, image.mode))

        super().__init__()

        self.image = image
        self.filter_parameters = {}

    @abstractmethod
    def apply(self):
        self.set_missing_filter_parameters()

        # Rest of function must be implemented by the concrete child class, calling super().apply()

    def set_filter_parameter(self, attribute_name: str, value):
        self.filter_parameters[attribute_name] = value

    def get_parameter_value(self, callback_function_to_validate_param, input_value):
        # In order to get the parameter value, we need to call the callback function defined for that parameter passing
        # as a parameter the input value that comes from an interactive input by the user or by the command line.
        if isinstance(callback_function_to_validate_param, staticmethod):
            # If the callback function is a static method, we need to get the underlying function, so we use .__func__
            method_to_validate_parameter = callback_function_to_validate_param.__func__

            return method_to_validate_parameter(input_value)

        method_to_validate_parameter = callback_function_to_validate_param

        return method_to_validate_parameter(self, input_value)

    def get_original_image_path_without_extension(self) -> str:
        return os.path.splitext(self.image.filename)[0]

    def get_original_image_extension(self) -> str:
        return os.path.splitext(self.image.filename)[1].replace('.', '')

    def save_image(self, image: Image, image_description: str) -> None:
        # By default, the image will be saved on the same folder as the original image.
        image_path = "{}_{}.{}".format(self.get_original_image_path_without_extension(), image_description,
                                       self.get_original_image_extension())

        image.save(image_path)

    def set_missing_filter_parameters(self):
        """ In case no filter parameters have been set and there are parameters required, this function will ask the
            user to input those values."""

        if not self.filter_parameters:
            for data in self.extra_data_required:
                param_name = data['attribute_name']

                while True:
                    try:
                        input_value = input(">> Enter a {} value: ".format(param_name))

                        param_value = self.get_parameter_value(data['callback_function'], input_value)

                        self.set_filter_parameter(param_name, param_value)

                        break
                    except Exception as e:
                        print(e)