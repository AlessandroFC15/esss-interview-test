import sys
from filters.filter import Filter
from filters.blur_filter import BlurFilter
from filters.rgb_split_filter import RGBSplitFilter
from filters.grayscale_filter import GrayscaleFilter
from PIL import Image


def get_input_args(args_list: list) -> dict:
    """ This method attempts to extract information from the list of commmand-line arguments, specifically the action
    desired, that must be one of '-blur' or '-rgb' and the filename.

    :param args_list: a list of command-line arguments passed to the script. It is expected that the name of file is
    not present in this list
    :return: a dictionary containg the action (-blur or -rgb), the path to the file and depending on the action, a
    radius and a weight value.
    """
    if len(args_list) < 1:
        raise ValueError("At least one argument must be provided")

    input_arguments = {
        'chosen_filter': None,
        'filename': None
    }

    # If there is only one argument, we expect it to be the filename.
    if len(args_list) == 1:
        input_arguments['filename'] = args_list[0]
        return input_arguments

    filters_command_line_flags = ["-" + filter.command_line_flag for filter in Filter.__subclasses__()]

    # If there is one more than one argument, we expect the first argument to be a flag indicating the filter to apply
    if args_list[0] not in filters_command_line_flags:
        raise ValueError("First argument must be one of {}, followed by a path to a image file".format(filters_command_line_flags))

    filter_name = args_list[0].replace('-', '')
    chosen_filter = next(filter for filter in Filter.__subclasses__() if filter.command_line_flag == filter_name)

    input_arguments['filename'] = args_list[1]
    input_arguments['chosen_filter'] = chosen_filter
    input_arguments['filter_parameters'] = get_extra_input_arguments(input_arguments['chosen_filter'], args_list)

    return input_arguments


def get_extra_input_arguments(filter: Filter, arguments_list: list) -> dict:
    """ This method attempts to extract information from the list of commmand-line arguments, apart from the already
        extracted filename and chosen filter. So this just extract the extra parameters that may be necessary for the
        filter.

        :param filter: The Filter class extracted early on from the command line
        :param arguments_list: a list of command-line arguments passed to the script. It is expected that the name of file is
        not present in this list.
        :return: a dictionary that has as keys the attribute names defined in 'extra_data_required' for that particular
        filter and for each attribute, a string as its value.
        """
    filter_parameters = {}
    input_index = 2

    for required_parameter in filter.extra_data_required:
        param_name = required_parameter['attribute_name']

        try:
            param_value = Filter.get_parameter_value(filter, required_parameter['callback_function'],
                                                     arguments_list[input_index])

            filter_parameters[param_name] = param_value
        except IndexError:
            raise ValueError("The attribute {} must be provided as an argument".format(param_name))

        input_index += 1

    return filter_parameters


def get_list_available_filters_based_on_image_type(image: Image) -> list:
    list_all_filters = Filter.__subclasses__()

    return [filter for filter in list_all_filters if image.mode in filter.image_types]


def print_available_filters(available_filters: list) -> None:
    print("\nAvaliable filters:")

    for i, filter in enumerate(available_filters):
        print("{}. {}".format(i + 1, filter.name))


def get_filter_choice(available_filters: list) -> int:
    while True:
        try:
            filter_choice = int(input("\nType the selected filter number: "))

            if 1 <= filter_choice <= len(available_filters):
                return filter_choice - 1

            print("## Invalid option ##\n")
        except ValueError as e:
            print("## Enter a valid integer ##\n")


if __name__ == "__main__":
    print(">> This is an Image App <<")

    try:
        input_args = get_input_args(sys.argv[1:])
    except ValueError as e:
        print(e)
        sys.exit(1)

    try:
        original_img = Image.open(input_args['filename'])
    except FileNotFoundError as e:
        print("# File {} was not found #".format(input_args['filename']))
        sys.exit(1)

    if input_args['chosen_filter']:
        # Instantiating a Filter children object
        filter_instance = input_args['chosen_filter'](original_img)

        for key, value in input_args['filter_parameters'].items():
            filter_instance.set_filter_parameter(key, value)
    else:
        list_available_filters = get_list_available_filters_based_on_image_type(original_img)

        if not list_available_filters:
            print('\n## Sorry, no filters are available for the image type {}. ##'.format(original_img.mode))
            sys.exit(1)

        print_available_filters(list_available_filters)

        filter_choice = get_filter_choice(list_available_filters)

        selected_filter = list_available_filters[filter_choice]

        print("\nSelected Filter: {}".format(selected_filter.name))

        # Instantiating a Filter children object
        filter_instance = selected_filter(original_img)

    filter_instance.apply()

    print("\n>> Image file processed successfully! <<")

    sys.exit(0)
